#!/bin/bash
mvn -f ../light-admin/lightadmin-core clean package
cp ../light-admin/lightadmin-core/target/lightadmin-1.1.0.BUILD-SNAPSHOT.jar jars/lightadmin.jar
./rep-maker.py jars 1.1.0.BUILD-SNAPSHOT
chmod +x mvn_commands
./mvn_commands
git add .
git commit -am "Update lightadmin lib"
git push origin master